
*****************
Installing on OSX
*****************

Check the :doc:`Installing Blender </getting_started/installing_blender/installing>`
page to find the minimum requirements and where to get Blender (if you haven't done so yet).

After downloading Blender for Mac-OSX, uncompress the file and drag ``blender.app`` onto the Applications folder.

.. tip::

   Because *Blender* doesn't use the standard OS menu system, you likely have a redundant menu-bar at the top.

   To remove it see `this post <http://www.macworld.com/article/55321/2007/02/hidemenubar.html>`__
   on Macworld, but beware that it is somewhat complex. As an alternative: simply make *Blender*
   full screen by :kbd:`Alt-F11` or by :menuselection:`File --> Window --> Toggle Window Fullscreen`.
