.. _window-system-index:

#################
  Window System
#################

.. toctree::
   :maxdepth: 1

   introduction.rst
   arranging_areas.rst
   headers.rst
   console_window.rst
